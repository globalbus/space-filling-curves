function [riceParameter, recursiveRice, avg]=calcParameter(im, recursiveRice)
avg=sum(reshape(im, [], 1))/numel(im);
riceParameter=ceil(2^floor(log2(avg)));
if(riceParameter<4)
    recursiveRice=false;
end
disp(['avg ' num2str(avg)]);
disp(['Rice Parameter ' num2str(riceParameter)]);