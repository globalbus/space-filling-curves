function [output] = riceCoding(input, dict, lengths, recursiveRice)
input=reshape(input, [], 1);
%maxLength=length(dict{end,2});
%output=zeros(1, length(input)*lengths(end), 'uint8');
%don't be too pessimistic, compressed length should be smaller than input
%length, otherways code can slow a little.
output=zeros(length(input)*8, 1,  'uint8');
%output=[];
offset=1;
reserved=int32(recursiveRice);
for i=1:length(input)
    value=int32(input(i));
    len=lengths(value+reserved+1);
    output(offset:offset+len-1)=dict(value+1+reserved, 1:len);
    offset=offset+len;
end
%add ones at the end of signal
addOnes=8-mod(offset-1, 8);
if(addOnes<8)
    output(offset:offset+addOnes-1)=ones(addOnes, 1, 'uint8');
    offset=offset+addOnes;
end
%strip unused rest
if(offset<length(output))
    output(offset:end)=[];
end
