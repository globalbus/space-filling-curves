function [image, LUT] = compressHistogram(input)
base=2^getBitsPerSample(input);
LUT=uniqueIntFast(input);
if(length(LUT)<base*0.1)%to be changed
    xy=size(input);
    %revLUT=reverseLUT(LUT, 0);
    revLUT=LUT;
    revLUT(revLUT+1)=0:(length(revLUT)-1);
    input=reshape(input, [],1);
    image= revLUT(input+1);
    image= reshape(image, xy);
else
    LUT=[];
    image=input;
end
