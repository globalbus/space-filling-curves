function [ux] = uniqueIntFast(input)
ux = cast(find(accumarray(cast(reshape(input, [], 1), 'int32')+1,1))-1, class(input));