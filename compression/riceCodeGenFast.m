function [output, lengths] = riceCodeGenFast(riceParameter,length1, recursiveRice)
n=log2(riceParameter);
bin=zeros(riceParameter,n, 'uint8');
%maxLength=ceil(length1/riceParameter+log2(riceParameter)+1);
%maxLength=ceil(sqrt(length1/riceParameter)*2+log2(riceParameter)+1);
if(recursiveRice)
    maxLength=calculateLength(riceParameter, length1);
else
    maxLength=ceil(length1/riceParameter+log2(riceParameter)+1);
end
for i=1:riceParameter
   temp=bitget(i-1, 1:n);
   %temp=de2bi(i-1, n);
   bin(i, :)=temp;
end
output=zeros(length1, maxLength, 'uint8');
lengths=zeros(1, length1);
for i=1:length1
   repeat=floor((i-1)/riceParameter);
   if(riceParameter<=2 || repeat<(n+1+lengths(repeat+2)) ||~recursiveRice)
       temp=[ones(1,repeat) 0 bin(mod(i-1,riceParameter)+1, :)];
   else
       temp=[0 bin(1, :) output(repeat+2,1:lengths(repeat+2)) bin(mod(i-1,riceParameter)+1, :)];
   end
   lengths(i)=length(temp);
   output(i, 1:lengths(i))=temp;
end

function length1 = calculateLength(riceParameter, value)
length1=log2(riceParameter)+1;
if(value>riceParameter)
    lengthOfUnary=ceil(value/riceParameter)-1;
    if(riceParameter>2)
        temp=calculateLength(riceParameter, lengthOfUnary+1)+length1-1;
        if(temp<lengthOfUnary)
            length1=length1+temp;
        else
            length1=length1+lengthOfUnary;
        end
    end
end