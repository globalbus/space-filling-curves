function [im]=imread2(filename)
[path, ~, ~] = fileparts(mfilename('fullpath'));
addpath(fullfile(path,'..', 'curveGenerators'));
tic
[encodedImage, encLUT]=readFromFileRice(filename);
im=riceMexDecoderWrapper(encodedImage.signal, encodedImage.orgSize, encodedImage.param, encodedImage.bitsPerSample, encodedImage.recursiveRice);
%im=riceDecoding(signal, orgSize, riceParameter, bitsPerSample, recursiveRice);
clear signal;
im=decodeLUT(im, encLUT);
im=reverseTransform(im,encodedImage.orgSize, encodedImage.bitsPerSample, encodedImage.curveType);
%postprocessing

toc