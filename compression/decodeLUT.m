function [im, decLUT]=decodeLUT(im, encLUT)
if(encLUT.param~=0)
    im=reshape(im, [],1);
    bitsPerSample = getBitsPerSample(im);
    decLUT=riceMexDecoderWrapper(encLUT.LUT, [encLUT.size, 1, 1], encLUT.param, bitsPerSample, 0);
    decLUT=[decLUT(1); decLUT(2:end)+1];
    decLUT=cumsumMexWrapper(decLUT, 2^bitsPerSample-1,class(decLUT));
    im = decLUT(im+1);
end