function [output] = transform(im, bitsPerSample, curve)
%take size
orgSize=size(im);
%curve type selection
[x, y]=selectCurve(orgSize, curve);

curveVector=getIndices(im, x, y, 1); %vectorize by curve
if length(orgSize)>=3
    curveVector=reshape(curveVector,[], orgSize(3)); %vector of pixels
else
    curveVector=reshape(curveVector,[], 1); %vector of pixels
end
output=transform2(curveVector, bitsPerSample);