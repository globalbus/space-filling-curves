function signal = riceMexDecoderWrapper(input, originalSize, riceParameter, bitsPerSample, recursiveType)
if checkMex()
input= reshape(input, [], 1);
bitsPerSample = uint8(bitsPerSample);
riceParameter = uint16(riceParameter);
recursiveRice = logical(recursiveType);
signal = riceMexDecoder(input, bitsPerSample, riceParameter,  recursiveRice);
signal = reshape(signal,[], originalSize(3));
else
   signal = riceDecoding(input, originalSize, riceParameter, bitsPerSample, recursiveType);
end
