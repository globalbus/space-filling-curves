function [signal, format] = riceMexCoderWrapper(im, riceParameter, recursiveRice)
bitsPerSample = getBitsPerSample(im);
if checkMex()
im= reshape(im, [], 1);
riceParameter = uint16(riceParameter);
recursiveRice = logical(recursiveRice);
signal = riceMexCoder(im, riceParameter, recursiveRice);
format = 8;

else
    [riceCode, lengths]=riceCodeGenFast(riceParameter, 2^bitsPerSample+1, recursiveRice);
    signal=riceCoding(im, riceCode, lengths, recursiveRice);
    format=1;
end
