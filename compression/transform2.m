function [output] = transform2(vector, bitsPerSample)
% orgSize=size(vector);
% previous=zeros(1,orgSize(2), 'int32');
% output=zeros(orgSize(1), orgSize(2), 'int32');
% for i=1:orgSize(1);
%     %get pixel color
%      actual=int32(vector(i, :));
%     difference=actual-previous;
%     for j=1:orgSize(2)
%     if(difference(j)>127)
%         difference(j)=difference(j)-256;
%     elseif (difference(j)<-127)
%         difference(j)=difference(j)+256;
%     end
%     end
%     for j=1:orgSize(2)
%     if(difference(j)>=0)
%         difference(j)=difference(j)*2;
%     else
%         difference(j)=-difference(j)*2-1;
%     end
%     end
%     previous=actual;
%     output(i, :)=difference;
% end

%do the same as commented code above, but really really faster.
type1=class(vector);
type2=[type1(2:4), num2str(str2num(type1(5:end))*2)];

vector=cast(vector, type2);
output=[vector(1, :); vector(2:end, :)-vector(1:end-1, :)];

%LUT=int32([1:127 -128:127 -128:-1]); 
%LUT2=int32([255:-2:1 0:2:254]);
maximumSigned=2^(bitsPerSample-1)-1;
minimumSigned=-2^(bitsPerSample-1);
LUT=cast([1:maximumSigned minimumSigned:maximumSigned minimumSigned:-1], type2);
maximumUnsigned=2^bitsPerSample-1;
LUT2=cast([maximumUnsigned:-2:1 0:2:maximumUnsigned-1], type2);

%output=LUT(output+256);
%output=LUT2(output+129);
LUT3=cast(LUT2(LUT+2^(bitsPerSample-1)+1), type1);

output=LUT3(output+2^bitsPerSample);
%output=cast(output, type1);