/*
 * AbstractBits.h
 *
 *  Created on: 25 cze 2015
 *      Author: globalbus
 */

#ifndef ABSTRACTBITS_H_
#define ABSTRACTBITS_H_
#include <iostream>
#include <cstdint>
#include <deque>
#include "Commons.h"
using namespace std;

namespace rice_coder {

class AbstractBits {
public:
	AbstractBits();
	virtual ~AbstractBits();
protected:
	const unsigned int bufferSize = 8192;
	uint8_t* buffer;
	unsigned int ptr =0 ;
};

} /* namespace rice_coder */

#endif /* ABSTRACTBITS_H_ */
