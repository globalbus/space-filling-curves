/*
 * AbstractBits.cpp
 *
 *  Created on: 25 cze 2015
 *      Author: globalbus
 */

#include "AbstractBits.h"

namespace rice_coder {

AbstractBits::AbstractBits() {
	buffer = new uint8_t[bufferSize];
}

AbstractBits::~AbstractBits() {
	delete[] buffer;
}

} /* namespace rice_coder */
