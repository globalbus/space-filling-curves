/*
 * WriteBits.h
 *
 *  Created on: 25 cze 2015
 *      Author: globalbus
 */

#ifndef WRITEBITS_H_
#define WRITEBITS_H_
#include "AbstractBits.h"
#include <vector>
namespace rice_coder {

class WriteBits: public AbstractBits {
public:
	virtual ~WriteBits();
	WriteBits(deque<uint8_t> & writer);
	virtual void pushBit(uint8_t bit);
	virtual void pushBits(vector<uint8_t> bits);
	virtual void pushByte(uint8_t byt);
	virtual void flush();
protected:
	deque<uint8_t> & stream;
	static inline void byteToBits(uint8_t & saveTo, uint8_t byte);
	virtual void saveBuffer();
private:
};

} /* namespace rice_coder */

#endif /* WRITEBITS_H_ */
