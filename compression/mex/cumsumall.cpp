# include "mex.h"
# include "matrix.h"
# include <cstdint>
// CUMSUMALL.CPP 
// templatized and modified to perform reverse transform on images
// This is a C++ code written to allow MATLAB to perform cumsumming on 
// integer data types.  This file was written as a companion to the 
// COMBINATOR M-File.  Integer data types that are allowed:
//                     int8, int16, int32.
// Note that this file has very little practical value outside of the use
// with COMBINATOR, because of the ease with which these data-types could
// overflow in a random context.  
// When using this file with COMBINATOR, N should be less than the maximum
// variable size allowed by the specific type.  For instance, when using 
// int8, N should be less than 127.
//                               combinator(int8(30),3,'p','r');
template<typename IN, typename OUT>
void cs(const mxArray *inp,const mxArray *oup,int row, int col)
{
    IN *INP;
    OUT *OUP;
    int hh;
    INP = (IN *)mxGetData(inp);
    OUP = (OUT *)mxGetData(oup);
    // Copy the first row.    
    for (int jj = 0; jj < col; jj++)
        *(OUP + row*jj ) =  *(INP + row*jj );
    // Perform cumsumming.
    for (int ii = 1; ii<row; ii++)
    {
        for (int jj = 0; jj < col; jj++)
        {
            hh = row*jj + ii; // Does this really save time?
            *(OUP + hh) = *(OUP + hh-1) + *(INP + hh);
        }
    } 
}

void mexFunction( int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
    mxClassID   category;
    const int *dm;  // Rows and Columns.
    int nd; // Number of dimensions.
    
    category = mxGetClassID(prhs[0]);
    nd = mxGetNumberOfDimensions(prhs[0]);
    dm = mxGetDimensions(prhs[0]);
    //  set the output pointer to the output matrix
    
    //  Call the other functions to do the cumsumming.
    // Several classes could be supported, but only three are used.
    switch (category)
    {
        case mxINT8_CLASS:
        plhs[0] = mxCreateNumericArray(nd,dm,mxUINT8_CLASS,mxREAL);
        cs<int8_t, uint8_t>(prhs[0],plhs[0],dm[0],dm[1]);  break;
         case mxUINT8_CLASS:
  plhs[0] = mxCreateNumericArray(nd,dm,mxUINT8_CLASS,mxREAL);
  cs<uint8_t, uint8_t>(prhs[0],plhs[0],dm[0],dm[1]);  break;
        case mxINT16_CLASS:
  plhs[0] = mxCreateNumericArray(nd,dm,mxUINT16_CLASS,mxREAL);
  cs<int16_t, uint16_t>(prhs[0],plhs[0],dm[0],dm[1]);  break;
        case mxUINT16_CLASS:
   plhs[0] = mxCreateNumericArray(nd,dm,mxUINT16_CLASS,mxREAL);
  cs<uint16_t, uint16_t>(prhs[0],plhs[0],dm[0],dm[1]);  break;           
        case mxINT32_CLASS:
  plhs[0] = mxCreateNumericArray(nd,dm,mxUINT32_CLASS,mxREAL);
  cs<int32_t,uint32_t>(prhs[0],plhs[0],dm[0],dm[1]);  break;
//         case mxUINT32_CLASS: cs_int32(prhs[0],plhs[0],dm[0],dm[1]);  break;
//         case mxSINGLE_CLASS: cs_single(prhs[0],plhs[0],dm[0],dm[1]);  break;
//         case mxDOUBLE_CLASS: cs_double(prhs[0],plhs[0],dm[0],dm[1]); break;
        default: break;
    }
}

// char -> int8, int -> int32, short -> int16, float -> single
