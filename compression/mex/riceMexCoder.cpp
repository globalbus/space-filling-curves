#include "mex.h"
#include "RiceCoder.h"
#include "RiceOnDemandCoder.h"
#include <exception>
using namespace std;
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	if (nrhs != 3) {
		mexErrMsgIdAndTxt("MATLAB:invalidNumInputs",
				"Invalid number of inputs");

	} else if (nlhs > 1) {
		mexErrMsgIdAndTxt("MATLAB:maxlhs", "Too many output arguments.");
	}
	const mxArray * matlabInput = prhs[0];
	uint riceCodeCount = 0;
	deque<uint8_t> stream;
	try {
		if (mxIsUint8(matlabInput)) {
			riceCodeCount = (1 << 8) + 1;
			uint8_t * inputMatrix = (uint8_t *) mxGetData(matlabInput);
			uint length = mxGetM(matlabInput);
			vector<uint8_t> input(inputMatrix, inputMatrix + length);
			uint16_t riceParameter = ((uint16_t *) mxGetData(prhs[1]))[0];
			bool recursive = ((bool *) mxGetData(prhs[2]))[0];
			rice_coder::WriteBits writer(stream);
			rice_coder::RiceCodeGenFast generator;
            generator.init(riceParameter, riceCodeCount,
					recursive);
			rice_coder::RiceCoder<uint8_t> coder;
			coder.code(input, writer, generator, recursive);
			plhs[0] = mxCreateNumericMatrix(stream.size(), 1, mxUINT8_CLASS,
					mxREAL);
			uint8_t * output = (uint8_t*) mxGetData(plhs[0]);
			std::copy(stream.begin(), stream.end(), output);
		} else if (mxIsUint16(matlabInput)) {
			riceCodeCount = (1 << 16) + 1;
			uint16_t * inputMatrix = (uint16_t *) mxGetData(matlabInput);
			uint length = mxGetM(matlabInput);
			vector<uint16_t> input(inputMatrix, inputMatrix + length);
			uint16_t riceParameter = ((uint16_t *) mxGetData(prhs[1]))[0];
			bool recursive = ((bool *) mxGetData(prhs[2]))[0];
			rice_coder::WriteBits writer(stream);
			rice_coder::RiceCoder<uint16_t> coder;
			if(riceParameter>4){
                    rice_coder::RiceCodeGenFast generator;
                    generator.init(riceParameter, riceCodeCount,
					recursive);
			coder.code(input, writer, generator, recursive);
            }
			else{ 
			rice_coder::RiceOnDemandCoder generator;
            generator.init(riceParameter, riceCodeCount,
					recursive);
			coder.code(input, writer, generator, recursive);
            }
			plhs[0] = mxCreateNumericMatrix(stream.size(), 1, mxUINT8_CLASS,
					mxREAL);
			uint8_t * output = (uint8_t*) mxGetData(plhs[0]);
			std::copy(stream.begin(), stream.end(), output);

		} else
			mexErrMsgIdAndTxt("MATLAB:type", "Invalid input type");
	} catch (exception e) {
        mexErrMsgIdAndTxt("MATLAB:error", e.what());
	}
}
