/*
 * Commons.h
 *
 *  Created on: 27 cze 2015
 *      Author: globalbus
 */

#ifndef COMMONS_H_
#define COMMONS_H_
#include <cstdint>
#include <exception>
using namespace std;
typedef unsigned int uint;
namespace rice_coder {

class EndOfFile : public exception{
public:
	EndOfFile(){};
};

template<typename T>
class Nullable {
public:
	Nullable(const T & value) :
			value(value) {
		this->null = false;
	}
	//Default constructor assumes a nullptr
	Nullable() {
		this->null = true;
	}

	//This handles the nullptr case
	Nullable(void* p) {
		this->null = true;
	}
	//Copy constructor
	Nullable(const Nullable& n);

	// move constructor
	Nullable(Nullable&& n) {
		this->null = n.null;
		if (!null)
			this->value = n.value;
	}

	// move assignment operator
	Nullable& operator=(Nullable&& n) {
		if (this != &n) {
			null = n.null;
			if (!null)
				this->value = n.value;
		}
	}

	/*** Overloaded Operators ***/

	operator T() {
		return value;
	}

	void operator =(void* p) {
		this->null = true;
	}

	const T operator =(const T& t) {
		this->null = false;
		return (value = t);
	}

	T& operator*() {
		return value;
	}

	bool operator ==(void* p) {
		if (p == nullptr) {
			if (null)
				return true;
		}
		return false;
	}

	bool operator !=(void* p) {
		return !operator ==(p);
	}

	bool isNull() {
		return this->null;
	}
	virtual ~Nullable() {
	}
	;
	T & get() {
		return value;
	}
	void set(T & value) {
		this->value = value;
	}
private:
	T value;
	bool null = true;
};

}

#endif /* COMMONS_H_ */
