/*
 * RiceCodeGenFast.h
 *
 *  Created on: 27 cze 2015
 *      Author: globalbus
 */

#ifndef RICECODEGENFAST_H_
#define RICECODEGENFAST_H_
#include <cmath>
#include <vector>
#include "Commons.h"
using namespace std;
namespace rice_coder {
class RiceCodeGenFast {
public:
	RiceCodeGenFast(){};
    virtual void init(uint riceParameter, uint length1,
			bool recursiveRice) {
		uint n = log2(riceParameter);
		uint maxLength;
		if (recursiveRice)
			maxLength = calculateLength(riceParameter, length1);
		else
			maxLength = ceil(length1 / riceParameter + log2(riceParameter) + 1);
		output.reserve(length1);
		uint8_t * lengths = new uint8_t[length1];
		std::fill(lengths, lengths+length1, 0);
		for (uint i = 0; i < length1; i++) {
			uint repeat = floor(i / riceParameter);
			vector < uint8_t > temp;
			temp.reserve(maxLength);
			if (riceParameter <= 2 || repeat < (n + 1 + lengths[repeat + 2])
					|| !recursiveRice) {
				addOnes(temp, repeat); //ones(1,repeat)
				temp.push_back(0); //0
				getBits(temp, i % riceParameter, n); //bin(mod(i-1,riceParameter)+1, :)
			} else {
				temp.push_back(0); //0
				getBits(temp, 0, n); //bin(1, :)
				temp.insert(temp.end(), output[repeat + 1].begin(),
						output[repeat + 1].end());
				//output(repeat+2,1:lengths(repeat+2))
				getBits(temp, i % riceParameter, n); //bin(mod(i-1,riceParameter)+1, :)
			}
			lengths[i] = temp.size();
			output.push_back(temp);
		}
		delete[] lengths;
	}
	virtual ~RiceCodeGenFast(){

	}
	virtual vector < uint8_t > getBitsForValue(uint value){
		return output[value];
	}
protected:
	vector < vector < uint8_t >> output;
	int calculateLength(uint riceParameter, uint value) {
		uint length1 = log2(riceParameter) + 1;
		if (value > riceParameter && riceParameter > 2) {
			uint lengthOfUnary = ceil(value / riceParameter) - 1;
			uint temp = calculateLength(riceParameter, lengthOfUnary + 1) + length1
					- 1;
			if (temp < lengthOfUnary)
				return length1 + temp;
			return length1 + lengthOfUnary;
		}
		return length1;
	}
	void addOnes(vector<uint8_t> & temp, int times) {
		for (int i = 0; i < times; i++)
			temp.push_back(1);
	}
	void getBits(vector<uint8_t> & saveTo, uint byte,
			uint significantBits) {
		for (int j = 1; j < 1 << significantBits; j = j << 1) {
			int value = (byte & j);
			int bit = (((-value) | value) >> (sizeof(int) * 8 - 1)) & 1;
			saveTo.push_back(bit);
		}
	}

};

} /* namespace rice_coder */

#endif /* RICECODEGENFAST_H_ */
