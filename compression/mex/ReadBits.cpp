/*
 * ReadBits.cpp
 *
 *  Created on: 27 cze 2015
 *      Author: globalbus
 */

#include "ReadBits.h"

namespace rice_coder {

ReadBits::ReadBits(vector<uint8_t> & input) :
		stream(input) {
	readBuffer();

}

ReadBits::~ReadBits() {
	// TODO Auto-generated destructor stub
}
bool ReadBits::eof() {
	if (ptr >= inBuffer) //sprawdź, czy jeszcze jest co czytać
		return endOfStream;
	else
		return false;
}
uint8_t ReadBits::getBit() throw(EndOfFile){
	if (ptr >= inBuffer) //sprawdź, czy jeszcze jest co czytać
		if (!endOfStream)
			readBuffer(); //wczytaj następne dane
		else
			throw EndOfFile();
	return buffer[ptr++];//wczytaj jeden bit//przejdź na następny
}

uint8_t ReadBits::getByte() throw(EndOfFile){
	if (ptr + 8 > inBuffer) //sprawdź, czy jeszcze jest co czytać
		if (!endOfStream)
			readBuffer(); //wczytaj następne dane
		else
			throw EndOfFile();
	int sb = 0;
	for (int j = 7; j >= 0; j--) {
		sb += (buffer[ptr++] & 1) << j;
	}
	return sb;
}
//
void ReadBits::readBuffer() {
	if (inBuffer > 0) {
		std::copy(buffer + ptr, buffer + inBuffer, buffer);
		ptr = inBuffer - ptr;
	}
	uint tempCapacity = (bufferSize - ptr) >> 3;
	uint8_t * tempBuffer = new uint8_t[tempCapacity];
	uint toRead;
	if (stream.size() - alreadyRead < tempCapacity) {
		toRead = stream.size() - alreadyRead;
		endOfStream = true;
	} else
		toRead = tempCapacity;
	std::copy(stream.begin() + alreadyRead,
			stream.begin() + alreadyRead + toRead, tempBuffer);
	alreadyRead += toRead;
	uint newPtr=ptr;
	for (uint i = 0; i < toRead; i++) {
		fromByteToBits(tempBuffer[i]);
	}
	ptr = 0;
	inBuffer = toRead<<3 + newPtr;
	delete[] tempBuffer;

}
void ReadBits::fromByteToBits(uint8_t & bytes) {  //zapis bajtu do tablicy bitów
	for (int j = 1; j < 0x100; j = j << 1) {
		int value = (bytes & j);
		int bit = (((-value) | value) >> (sizeof(int) * 8 - 1)) & 1;
		buffer[ptr++] = bit;
	}
}
} /* namespace rice_coder */
