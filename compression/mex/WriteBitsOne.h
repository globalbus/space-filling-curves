/*
 * WriteBitsOne.h
 *
 *  Created on: 27 cze 2015
 *      Author: globalbus
 */

#ifndef WRITEBITSONE_H_
#define WRITEBITSONE_H_
#include "WriteBits.h"
namespace rice_coder {

class WriteBitsOne : public WriteBits {
public:
	WriteBitsOne(deque<uint8_t> & writer):WriteBits(writer){};
	    void pushBit(uint8_t bit){
	    	stream.push_back(bit);
	    }
	    void pushBits(vector < uint8_t > bits){
	    	stream.insert(stream.end(), bits.begin(), bits.end());
	    }
	    void pushByte(uint8_t byt){
	    	for (int j = 0x80; j > 0; j = j >> 1) {
	    	    		int value = (byt & j);
	    	    		int bit = (((-value) | value) >> (sizeof(int) * 8 - 1)) & 1;
	    	    		stream.push_back(bit);
	    	    	}
	    }
	    void saveBuffer(){};
	    void flush(){
        uint toPush = 8-stream.size()%8;
        while(toPush--!=0)
			stream.push_back(1);
	}
	;
	virtual ~WriteBitsOne(){};
};

} /* namespace rice_coder */

#endif /* WRITEBITSONE_H_ */
