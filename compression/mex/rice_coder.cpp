//============================================================================
// Name        : rice_coder.cpp
// Author      : globalbus
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include<climits>
#include <deque>

#include "WriteBits.h"
#include "RiceCodeGenFast.h"
#include "RiceCoder.h"
#include "ReadBits.h"
#include <string>
using namespace std;


int main() {
	deque<uint8_t> stream;
	rice_coder::WriteBits writer(stream);
	rice_coder::RiceCodeGenFast generator(8, 257, 1);
	rice_coder::RiceCoder<int> coder;
	vector<int> input;
	for(int i=0; i<522240;i++)
		input.push_back(rand()%256);
	coder.code(input, writer, generator, 1);
	vector<uint8_t> toRead(stream.begin(), stream.end());
	rice_coder::ReadBits reader(toRead);
	vector<int> output;
	coder.decode(output, reader, 8, 1);
	for(int i=0; i<522240;i++)
			if(input[i]!=output[i])
				;

	return 0;
}

