/*
 * RiceCoder.h
 *
 *  Created on: 25 cze 2015
 *      Author: globalbus
 */

#ifndef RICECODER_H_
#define RICECODER_H_
#include "WriteBits.h"
#include "RiceCodeGenFast.h"
#include "ReadBits.h"
namespace rice_coder {
template<typename T>
class RiceCoder {
public:
	RiceCoder(){};
	virtual ~RiceCoder(){};
	void code(vector<T> & input, WriteBits & writer, RiceCodeGenFast& generator, bool recursiveRice){
        uint recursive=0;
        if(recursiveRice==1)
            recursive =1;
		for(uint i=0; i< input.size();i++){
			writer.pushBits(generator.getBitsForValue(input[i]+recursive));
		}
		writer.flush();
	}
	void decode(vector<T> & output, ReadBits & reader, uint riceParameter, bool recursiveRice){
		uint restLength=log2(riceParameter);
		try{
		while(!reader.eof()){
			//inlined readSymbol
			T outputValue=0;

			uint countOnes = 0;
			while(true){
				uint8_t bit = reader.getBit();
				if(bit==0)
					break;
				countOnes++;
			}
			T rest = getNBitsAndConvert(reader, restLength);
			if(countOnes==0 && rest==0 && recursiveRice)//goToRecursion
			{
				T recursive =readSymbol(reader, restLength, riceParameter, recursiveRice);
				if(reader.eof())
					return;
				rest = getNBitsAndConvert(reader, restLength);
				outputValue= recursive*riceParameter+(rest-1);
			}
			else
				outputValue= countOnes*riceParameter+rest-recursiveRice;
			output.push_back(outputValue);
		}
		}
		catch(EndOfFile){
			//just exit;
		}
	}
private:
	T getNBitsAndConvert(ReadBits & reader, uint & length){
		uint sb=0;
        for (uint j = 0; j < length; j++) {
        	uint8_t bit = reader.getBit();
            sb += (bit & 1) << j;
        }
		return sb;
	}
	uint readSymbol(ReadBits & reader,uint &restLength, uint& riceParameter, bool & recursiveRice){
		uint countOnes = 0;
		while(true){
			uint8_t bit = reader.getBit();
			if(bit==0)
				break;
			countOnes++;
		}
		T rest = getNBitsAndConvert(reader, restLength);
		if(countOnes==0 && rest==0 && recursiveRice)//goToRecursion
		{
			T recursive =readSymbol(reader, restLength, riceParameter, recursiveRice);
			if(reader.eof())
				return 0;
			rest = getNBitsAndConvert(reader, restLength);
			return recursive*riceParameter+rest-1;
		}
		else
			return countOnes*riceParameter+rest-1;
	}
};


} /* namespace rice_coder */

#endif /* RICECODER_H_ */
