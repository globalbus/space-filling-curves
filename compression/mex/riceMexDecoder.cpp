#include "mex.h"
#include "RiceCoder.h"
#include <exception>
using namespace std;
//[riceCode, lengths]=riceCodeGenFast(riceParameter, 2^bitsPerSample+1, recursiveRice);
//signal=riceCoding(im, riceCode, lengths, recursiveRice);
//signal = riceMexCoder(im, riceParameter, recursiveRice)
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	if (nrhs != 4) {
		mexErrMsgIdAndTxt("MATLAB:invalidNumInputs",
				"Invalid number of inputs");

	} else if (nlhs > 1) {
		mexErrMsgIdAndTxt("MATLAB:maxlhs", "Too many output arguments.");
	}
	const mxArray * matlabInput = prhs[0];
	uint riceCodeCount = 0;
    uint8_t * inputMatrix = (uint8_t *) mxGetData(matlabInput);
    uint length = mxGetM(matlabInput);
	vector<uint8_t> input(inputMatrix, inputMatrix + length);
    uint8_t bitsPerSample = ((uint8_t *) mxGetData(prhs[1]))[0];
    rice_coder::ReadBits reader(input);
	try {
		if (bitsPerSample==8) {
            rice_coder::RiceCoder<uint8_t> coder;
            vector<uint8_t> output;
			uint16_t riceParameter = ((uint16_t *) mxGetData(prhs[2]))[0];
			bool recursive = ((bool *) mxGetData(prhs[3]))[0];
			coder.decode(output, reader, riceParameter, recursive);
			plhs[0] = mxCreateNumericMatrix(output.size(), 1, mxUINT8_CLASS,
					mxREAL);
			uint8_t * outputPtr = (uint8_t*) mxGetData(plhs[0]);
			std::copy(output.begin(), output.end(), outputPtr);
		} else if (bitsPerSample==16) {
            rice_coder::RiceCoder<uint16_t> coder;
            vector<uint16_t> output;
			uint16_t riceParameter = ((uint16_t *) mxGetData(prhs[2]))[0];
			bool recursive = ((bool *) mxGetData(prhs[3]))[0];
			coder.decode(output, reader, riceParameter, recursive);
			plhs[0] = mxCreateNumericMatrix(output.size(), 1, mxUINT16_CLASS,
					mxREAL);
			uint16_t * outputPtr = (uint16_t*) mxGetData(plhs[0]);
			std::copy(output.begin(), output.end(), outputPtr);
		} else
			mexErrMsgIdAndTxt("MATLAB:type", "Invalid input type");
	} catch (exception e) {
        mexErrMsgIdAndTxt("MATLAB:error", e.what());
	}
}