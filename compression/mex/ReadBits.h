/*
 * ReadBits.h
 *
 *  Created on: 27 cze 2015
 *      Author: globalbus
 */

#ifndef READBITS_H_
#define READBITS_H_
#include "AbstractBits.h"
#include <vector>
namespace rice_coder {

class ReadBits : AbstractBits {
public:
	ReadBits(vector<uint8_t> & input);
	virtual ~ReadBits();
	bool eof();
	uint8_t getBit() throw(EndOfFile);
	uint8_t getByte() throw(EndOfFile);
private:
	uint inBuffer=0;
	uint alreadyRead=0;
	vector<uint8_t> & stream;
	bool endOfStream = false;
	void readBuffer();
	void fromByteToBits(uint8_t & bytes);
};

} /* namespace rice_coder */

#endif /* READBITS_H_ */
