function compiler()
CXXFLAGS='-std=c++11 -D_GNU_SOURCE -fPIC -fno-omit-frame-pointer -O2 -finline-functions -march=native';
[path, ~, ~] = fileparts(mfilename('fullpath'));
files =dir(fullfile(path, '*.cpp'));
if(~isempty(files))
    prv =pwd;
    cd(path);
    options = ['CXXFLAGS=', CXXFLAGS];
    mex(options, 'riceMexCoder.cpp',  'AbstractBits.cpp', 'WriteBits.cpp', 'ReadBits.cpp');
    mex(options, 'riceMexDecoder.cpp',  'AbstractBits.cpp', 'WriteBits.cpp', 'ReadBits.cpp');
    mex(options,'cumsumall.cpp');
    cd(prv);
end