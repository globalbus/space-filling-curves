/*
 * WriteBits.cpp
 *
 *  Created on: 25 cze 2015
 *      Author: globalbus
 */

#include "WriteBits.h"

namespace rice_coder {

WriteBits::WriteBits(deque<uint8_t> & writer) :
		stream(writer) {
}

WriteBits::~WriteBits() {
}

void WriteBits::pushBit(uint8_t bit) {
	if (ptr >= bufferSize) //if next does not fit
		saveBuffer();
	buffer[ptr++] = bit;
}
void WriteBits::pushBits(vector<uint8_t> bits) {
	if (ptr + bits.size() >= bufferSize) //if next does not fit
		saveBuffer();
	std::copy(bits.begin(), bits.end(), buffer + ptr);
	ptr += bits.size();
}
void WriteBits::pushByte(uint8_t byt) {
	if (ptr + 8 >= bufferSize) //if next does not fit
		saveBuffer();
	byteToBits(*(buffer + ptr), byt);
}

void WriteBits::saveBuffer() {
	unsigned int i = 0;
	unsigned int to = (ptr >> 3) << 3; //x= x -(x mod 8);
	while (i < to) {
		int sb = 0;
		for (int j = 0; j < 8; j++) {
			sb += (buffer[i++] & 1) << j;
		}
		stream.push_back((uint8_t) sb);
	}
	std::copy(buffer + i, buffer + ptr, buffer);
	ptr -= i;
}
void WriteBits::flush() { //save rest of buffer
	saveBuffer();
	if (ptr!=0) {
		int sb = 0;
		uint i=0;
		for (int j = 0; j < 8; j++) {
			if (i < ptr) //if there is something in buffer
				sb += (buffer[i++] & 1) << j;
			else
				sb += 1 << j;
		}
		stream.push_back((uint8_t) sb);
	}

}
void WriteBits::byteToBits(uint8_t & saveTo, uint8_t byte) {
	uint8_t * ptr = &saveTo;
	for (int j = 0x80; j > 0; j = j >> 1) {
		int value = (byte & j);
		int bit = (((-value) | value) >> (sizeof(int) * 8 - 1)) & 1;
		*(ptr++) = bit;
	}
}
}
/* namespace rice_coder */
