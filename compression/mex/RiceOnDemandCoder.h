/*
 * RiceOnDemandCoder.h
 *
 *  Created on: 10 sie 2015
 *      Author: globalbus
 */

#ifndef RICEONDEMANDCODER_H_
#define RICEONDEMANDCODER_H_
#include "RiceCodeGenFast.h"

namespace rice_coder {

class RiceOnDemandCoder: public RiceCodeGenFast {
public:
    RiceOnDemandCoder(){};
	virtual void init(uint riceParameter, uint length1, bool recursiveRice) {
		this->riceParameter = riceParameter;
		this->recursiveRice = recursiveRice;
	}
	virtual vector<uint8_t> getBitsForValue(uint i) override {
		uint n = log2(riceParameter);
		uint repeat = floor(i / riceParameter);
		vector<uint8_t> temp;
		addOnes(temp, repeat); //ones(1,repeat)
		temp.push_back(0); //0
		getBits(temp, i % riceParameter, n); //bin(mod(i-1,riceParameter)+1, :)
		return temp;
	}
	virtual ~RiceOnDemandCoder(){};
private:
	uint riceParameter;
	bool recursiveRice;
};

} /* namespace rice_coder */

#endif /* RICEONDEMANDCODER_H_ */
