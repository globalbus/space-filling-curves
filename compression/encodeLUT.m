function [encLUT] = encodeLUT(LUT)
if(isempty(LUT))
    encLUT=struct('LUT', [], 'param', 0, 'format', 0, 'size', 0);
else
%estimator
encLUT=[LUT(1, :); LUT(2:end, :)-LUT(1:end-1, :)-1];
riceParameter=calcParameter(encLUT);
[encLUT, format] = riceMexCoderWrapper(encLUT, riceParameter, 0);
encLUT=struct('LUT', encLUT, 'param', riceParameter, 'format', format, 'size', length(LUT));
end