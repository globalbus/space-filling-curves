function saveToFileRice(filename, encodedImage, encLUT)
fileDesc=fopen(filename, 'w');
orgSize=encodedImage.orgSize;
if length(orgSize)<3
    orgSize=[orgSize 1];
end
%HEADER

%height
fwrite(fileDesc, orgSize(1), 'uint16');
%width
fwrite(fileDesc, orgSize(2), 'uint16');
%number of colors
fwrite(fileDesc, orgSize(3), 'uint8');
riceParameter=log2(encodedImage.param)*2+encodedImage.recursiveRice;%encode information about recursion in least significant bit
%rice coding parameter (save as logarithm of 2)
fwrite(fileDesc, riceParameter, 'uint8');
%bitsPerSample
fwrite(fileDesc, encodedImage.bitsPerSample, 'uint8');
%bitsPerSample
fwrite(fileDesc, encodedImage.curveType, 'uint8');
% %LUT
fwrite(fileDesc, encLUT.param, 'uint8');
if(encLUT.param~=0)
fwrite(fileDesc, encLUT.size, 'uint16');
fwrite(fileDesc, length(encLUT.LUT), 'uint16');
if(encLUT.format==8)
    fwrite(fileDesc,encLUT.LUT,'uint8');
else
    fwrite(fileDesc,encLUT.LUT,'ubit1');
end
end

%DATA
%image data
if(encodedImage.format==8)
    fwrite(fileDesc,encodedImage.signal,'uint8');
else
    fwrite(fileDesc,encodedImage.signal,'ubit1');
end
fclose(fileDesc);