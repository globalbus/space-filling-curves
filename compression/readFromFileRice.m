function [encodedImage, encLUT]=readFromFileRice(filename)
if (exist(filename, 'file')~=2)
    error('file does not exists!');
end
fileDesc=fopen(filename, 'r');
orgSize=zeros(1,3);

%HEADER
%height
orgSize(1)=fread(fileDesc, 1, 'uint16');
%width
orgSize(2)=fread(fileDesc, 1, 'uint16');
%number of colors
orgSize(3)=fread(fileDesc, 1, 'uint8');
encodedImage.orgSize=orgSize;
%rice coding parameter (saved as logarithm of 2)
riceParameter=fread(fileDesc, 1, 'uint8');
encodedImage.recursiveRice = mod(riceParameter, 2);
encodedImage.param = 2^(floor(riceParameter/2));
%bitsPerSample
encodedImage.bitsPerSample=fread(fileDesc, 1, 'uint8');
%bitsPerSample
encodedImage.curveType=fread(fileDesc, 1, 'uint8');
% %LUT
 encLUT.param=fread(fileDesc, 1, 'uint8');
if(encLUT.param==0)
     encLUT.LUT=[];
else
      encLUT.size=fread(fileDesc, 1, 'uint16');
      lengthOfLUT=fread(fileDesc, 1, 'uint16');
      encLUT.LUT=fread(fileDesc, lengthOfLUT, 'uint8=>uint8');
end

%DATA
%read image
if checkMex()
    encodedImage.signal=fread(fileDesc,inf,'uint8=>uint8');
else
    encodedImage.signal=fread(fileDesc,inf,'ubit1=>uint8');
end
fclose(fileDesc);