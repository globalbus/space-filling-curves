function [avg]=imwrite2(im,filename, curveType, recursiveRice)
[path, ~, ~] = fileparts(mfilename('fullpath'));
addpath(fullfile(path,'..', 'curveGenerators'));
tic

if ~exist('curveType','var') || isempty(curveType)
    curveType=6; %hilbert curve by default
end
if ~exist('recursiveRice','var') || isempty(recursiveRice)
    recursiveRice=true; %recursive rice codes by default
end
bitsPerSample = getBitsPerSample(im);
%preprocessing

orgSize=size(im);
im=transform(im, bitsPerSample, curveType);
[im, LUT] = compressHistogram(im);
encLUT=encodeLUT(LUT);
%experimental formula
[riceParameter, recursiveRice, avg]=calcParameter(im, recursiveRice);


[signal, format] = riceMexCoderWrapper(im, riceParameter, recursiveRice);
clear im;
encodedImage = struct('signal', signal, 'format', format, 'curveType', curveType, 'orgSize', orgSize, 'param', riceParameter, 'bitsPerSample', bitsPerSample, 'recursiveRice', recursiveRice);
saveToFileRice(filename, encodedImage, encLUT);
toc
