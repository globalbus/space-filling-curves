function output = cumsumMexWrapper(A, maximumUnsigned, type1)
%if checkMex()
%    output=cumsumall(A);
%else
    output=cast(mod(cumsum(double(A)), maximumUnsigned+1), type1);
%end
