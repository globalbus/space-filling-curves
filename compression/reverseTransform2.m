function [output] = reverseTransform2(vector, bitsPerSample)
orgSize=size(vector);
% previous=zeros(1,orgSize(2), 'int32');
% if(bitsPerSample==8)
%     output=zeros(orgSize(1), orgSize(2), 'uint8');
% else
%     output=zeros(orgSize(1), orgSize(2), 'uint16');
% end
type1=['uint', num2str(bitsPerSample)];
type2=['int', num2str(bitsPerSample)];
type3=['int', num2str(bitsPerSample*2)];

maximumUnsigned=2^bitsPerSample-1;
LUT=cast([maximumUnsigned:-2:1 0:2:maximumUnsigned-1], type3);
revLUT=cast(reverseLUT(LUT,(maximumUnsigned+1)/2), type2);

vector=revLUT(cast(vector, type3)+1);
vector=reshape(vector, [], orgSize(2));
% for i=1:orgSize(1);
%      %get pixel color
%     difference=int32(vector(i, :));
% %     for j=1:orgSize(2);
% %         if(mod(difference(j),2)==0)
% %             difference(j)=difference(j)/2;
% %         else
% %             difference(j)=-(difference(j)+1)/2;
% %         end
% %     end
% 
%     actual=previous+difference;
%     actual=mod(actual, maximumUnsigned+1);
%     previous=actual;
%     output(i, :)=actual;
% end
output=cumsumMexWrapper(vector, maximumUnsigned, type1);