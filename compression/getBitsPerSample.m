function bits = getBitsPerSample(im)
if isa(im,'uint8')
    bits=8;
elseif isa(im, 'uint16')
    bits=16;
else
    error('format not supported, convert to uint8 or uint16');
end