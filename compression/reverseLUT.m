function [LUT] = reverseLUT(LUT, padding)
LUT(LUT+1)=-padding:length(LUT)-padding-1;