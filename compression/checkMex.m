function [ bool ] = checkMex()
[path, ~, ~] = fileparts(mfilename('fullpath'));
addpath(fullfile(path,'mex'));
if ~check()
    compiler();
end
bool =  check();
end

function bool = check()
array = {'riceMexCoder','riceMexDecoder','cumsumall'};
bool=true;
for i=array
    if(~exist(fullfile('mex',[cell2mat(i),'.', mexext]), 'file'))
        bool=false;
        break;
    end
end
end