function [output, dict] = huffmanCoding(input)
input=reshape(input, [], 1);
symbols=unique(input);
alf=histc(input, symbols);
siz=size(input);
dict=huffmandict(symbols, alf./siz(1));
output=huffmanenco(input, dict);
