function [output] = riceDecoding(input, originalSize, riceParameter, bitsPerSample, recursiveType)
input=reshape(input, [], 1);
%input=double(input);%to do faster binary to decimal conversion
%size of decoding output was known, so prepare buffer
if(bitsPerSample==8)
    output=zeros(originalSize, 'uint8');
else
    output=zeros(originalSize, 'uint16');
end
output=reshape(output, [], 1);


restLength=log2(riceParameter);
restMask=1:restLength;
bits=bitset(0,restMask,ones(1,restLength));
outputOffs=1;
startOffs=1;
reserved=recursiveType;
while outputOffs<=length(output);
    %inlined decodeSymbol for improved performance
    endOffs=startOffs;
    while input(endOffs)~=0
        endOffs=endOffs+1;
    end
    rest=bits*double(input(endOffs+1:endOffs+restLength));%a little bit faster
    if(startOffs==endOffs && rest==0 && reserved)
        startOffs=endOffs+restLength+1;
        [recursive]=decodeSymbol();
        rest=bits*double(input(startOffs:startOffs+restLength-1));
        startOffs=startOffs+restLength;
        output(outputOffs)=recursive*riceParameter+rest-reserved;
    else
        output(outputOffs)=(endOffs-startOffs)*riceParameter+rest-reserved;
        startOffs=endOffs+restLength+1;
    end
    %[output(outputOffs)]=decodeSymbol();
    outputOffs=outputOffs+1;
end
output=reshape(output,[], originalSize(3));

    function [output2]=decodeSymbol()
        endOffs=startOffs;
        while input(endOffs)~=0
            endOffs=endOffs+1;
        end
        rest=bits*double(input(endOffs+1:endOffs+restLength));%a little bit faster
        if(startOffs==endOffs && rest==0)
            startOffs=endOffs+restLength+1;
            [recursive]=decodeSymbol();
            rest=bits*double(input(startOffs:startOffs+restLength-1));
            startOffs=startOffs+restLength;
            output2=recursive*riceParameter+rest-reserved;
        else
            output2=(endOffs-startOffs)*riceParameter+rest-reserved;
            startOffs=endOffs+restLength+1;
        end
    end
end