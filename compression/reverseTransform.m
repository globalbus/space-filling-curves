function [im] = reverseTransform(input, orgSize, bitsPerSample, curveType)
%curve type selection
[x, y]=selectCurve(orgSize, curveType);

output=reverseTransform2(reshape(input,[],orgSize(3)), bitsPerSample);
clear input;
im=getIndices(reshape(output, orgSize), x, y, -1); %vectorize by curve