function [x,y] = genericCurve(n, prod, helpVect)
%Generic curve generator
if(n<8)
    type='int8';
elseif(n<16)
    type='int16';
%elseif(n<32) that size is totally useless and eats a GB of RAM.
%    type='int32';
else
    error('curve request is too large');
end
%checks
[helpDim,cubeDim]=size(helpVect);
[prodOpt,prodDim]=size(prod);
if helpDim~=2 || prodOpt~=4 || cubeDim~=prodDim
    error('invalid input dimensions')
end

absProd=abs(prod);
one=(absProd(1,:)+absProd(2,:)).*(absProd(1,:)+absProd(3,:)).*(absProd(2,:)+absProd(4,:));
if ~isequal(sum(helpVect,2), zeros(2,1)) || ~isequal(one, ones(1,cubeDim))
    error('invalid input data');
end

sDim=sqrt(cubeDim);
help=sDim^n;
x=zeros(help^2,1, type);
y=zeros(help^2,1, type);
xo=zeros((help/sDim)^2,1, type);
yo=zeros((help/sDim)^2,1, type);
offset=floor(help^2/2);
help=cast(help, type);

prod=cast(prod, type);
helpVect=cast(helpVect, type);

previous=1;
for i=1:n-1
    actual=ceil(sDim^i^2/2);
    x(offset-actual+sDim-1:offset+actual)=(reshape(repmat(prod(1,:),previous,1),[], 1).*repmat(xo(1:previous),cubeDim,1)+reshape(repmat(prod(2,:),previous,1), [], 1).*repmat(yo(1:previous),cubeDim,1)+reshape(repmat(helpVect(1,:),previous,1), [], 1)*help)/sDim;
    y(offset-actual+sDim-1:offset+actual)=(reshape(repmat(prod(3,:),previous,1),[], 1).*repmat(xo(1:previous),cubeDim,1)+reshape(repmat(prod(4,:),previous,1), [], 1).*repmat(yo(1:previous),cubeDim,1)+reshape(repmat(helpVect(2,:),previous,1), [], 1)*help)/sDim;
    previous=sDim^i^2;
    xo(1:previous)=x(offset-actual+sDim-1:offset+actual);
    yo(1:previous)=y(offset-actual+sDim-1:offset+actual);
end
x=(reshape(repmat(prod(1,:),previous,1),[], 1).*repmat(xo,cubeDim,1)+reshape(repmat(prod(2,:),previous,1), [], 1).*repmat(yo,cubeDim,1)+reshape(repmat(helpVect(1,:),previous,1), [], 1)*help)/sDim;
y=(reshape(repmat(prod(3,:),previous,1),[], 1).*repmat(xo,cubeDim,1)+reshape(repmat(prod(4,:),previous,1), [], 1).*repmat(yo,cubeDim,1)+reshape(repmat(helpVect(2,:),previous,1), [], 1)*help)/sDim;

