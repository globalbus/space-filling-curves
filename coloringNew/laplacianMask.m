function mask=laplacianMask(weight, size)
mask=weight.^(linspace(-1,-floor(size),floor(size)))
mask=mask./sum(mask);
mask=[mask(mask(2:end)>2*eps)];
