function [output, LUT] = coloring2(im, curve, feedback, weight, lutPrecision, colors)
% output     output image
% im         input image
% curve      curve id (allowed 1 and 2)
% feedback   feedback factor (from 0 to 1)
% mod        weight factor
% func       dimension function
% colors     allowed colors array
[path, ~, ~] = fileparts(mfilename('fullpath'));
addpath(fullfile(path,'..', 'curveGenerators'));
tic
%take size
orgSize=size(im);
%curve type selection
[x, y]=selectCurve(orgSize, curve);
toc
tic


curveVector=getIndices(im, x, y, 1); %vectorize by curve
curveVector=reshape(curveVector, [], orgSize(3)); %vector of pixels
mask=laplacianMask(weight, feedback);
%mask=gaussMask(weight, feedback);

colors=unique(colors,'rows');
%dt=DelaunayTri(colors);
[~,v1]=convhulln(double(curveVector));
[~,v2]=convhulln(double(colors));
reproductionRate=v2/v1
tri   = delaunayn(colors, {'Qt','Qbb','Qc', 'Qz'});
lutPrecision=ceil(2^floor(log2(lutPrecision)));
lin=0:min(lutPrecision, 256)-1;
[p,q,z] = meshgrid(lin, lin, lin);
LUT = [q(:) p(:) z(:)];
divider=256/lutPrecision;
colorsToLUT=ceil(colors./divider);
idx = dsearchn(colorsToLUT, tri, LUT);
%idx=nearestNeighbor(dt, LUT);
LUT=reshape(uint8(idx), [lutPrecision lutPrecision lutPrecision]);
clear idx;



[s, ~]=size(colors);
%feedback vector
E=zeros(length(mask),3);
%helper for colors distance
% nor=zeros(1,s);
% func=@(x)norm(x);
for i=1:(length(curveVector));
    %get pixel color
    temp=double(curveVector(i, :));
    %add feedback
    e=temp+mask*E;
%    e=temp+E(1, :);
    %calculate distance from colors in array to pixel color
%         for j=1:s
%             nor(j)=func(e-colors(j,:));
%         end
%         h1=repmat(e,s,1)-colors;
%         nor=sqrt(sum(h1.^2,2));
%     %take color with minimum distance
%     [~,I]=min(nor);
    %idx = dsearchn(colors, tri, e);
    %    e=max(min(round(e./divider), [lutPrecision-1 lutPrecision-1 lutPrecision-1]), [0 0 0])+1;
    e=max(min(round((e+1)./divider), [lutPrecision lutPrecision lutPrecision]), [1 1 1]);
    
    idx=LUT(e(1), e(2), e(3));
    %idx=nearestNeighbor(dt, e);
    A=colors(idx, :);
    %save value in image
    curveVector(i, :)=A;
    %prepare feedback value
    
    E=[temp-A;E(1:end-1, :);];
%    E=[zeros(1,3); E(1:end-1, :)];
%    E=E+repmat((temp-A), length(mask),1).*repmat(mask', 1, 3);
    %mask=circshift(mask',1)';
    %mask=[mask(end) mask(1:end-1)];
    %E(mod(i, length(mask))+1, :)=temp-A;
    
end
toc
output=getIndices(reshape(curveVector, orgSize), x, y, -1); %vectorize by curve
