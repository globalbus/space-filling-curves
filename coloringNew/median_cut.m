% Kwantyzacja kolorów metoda median cut.
%
% nowa_paleta = median_cut(obraz, liczba_kolorow);
%

function pal = median_cut(in, num);

block_num = 1;

% bloki przechowywane sa jako struktury
% pierwszy blok zawiera wszystkie kolory
%blocks.([bln, int2str(block_num)]) = in;
blocks={in};
[Cmin]=getMaxDiff3(in);
if(num<8)
half=floor(num/2);
pal=Cmin([1:half end-half+1:end],:);
else
    pal=Cmin;
end
usedColors=8;
while usedColors < num
    m_len = 0; % dlugosc najdluzszego boku
    m_idx = 0; % indeks koloru dla tego boku
    m_i = 0;   % indeks bloku z najdluzszym bokiem
    
    % szukamy najdluzszego boku wsrod wszystkch blokow
    for i = 1 : block_num
        cur = cell2mat(blocks(i));
        % dlugosci bokow dla trzech kanalow
        dif = max(cur) - min(cur);
        % dlugosc najdluzszego oraz indeks kanalu
        [m_len_, m_idx_] = max(dif);
        % zwyczajne poszukiwanie maksimum
        if m_len_ > m_len
            m_len = m_len_;
            m_i = i;
            m_idx = m_idx_;
        end
    end % for
    
    % wybrany blok
    max_block = cell2mat(blocks(m_i));
    
    % znajdujemy mediane
    max_block = sortrows(max_block, m_idx);
    med = floor((size(max_block, 1) + 1)/ 2);
    
    % dzielimy wybrany blok wg. mediany
    nblock1 = max_block(1 : med, :);
    nblock2 = max_block(med+1 : size(max_block, 1), :);
    
    % zamieniamy wybrany blok oraz dokladamy nowy
    blocks(m_i) = {nblock1};
    blocks(end+1)= {nblock2};
    [Cmin]=getMaxDiff3(nblock1);
    now=num-usedColors;
    set=Cmin(Cmin(:,m_idx)==max_block(med, m_idx), :);
    if(now<4)
    half=floor(now/2);
    set=set([1:half end-half+1:end],:);
    end;
    pal=[pal; set];
    usedColors=usedColors+4;
    block_num = block_num + 1;
end % while
return;
end
function [minPoint]=getMaxDiffSub(in)
[~, numberOfDim]=size(in);
[CMax, IMax] = max(in,[],  1);
[CMin,IMin] = min(in, [], 1);
if numberOfDim==1
    minPoint=CMin;
else
    minPoint=zeros(2^(numberOfDim-1), numberOfDim);
    [sortedX,sortingIndices] = sort(CMax-CMin,'descend');
    
    for i=1:2^(numberOfDim-1)
        IDiff=sortingIndices(ceil(i/2^(numberOfDim-1)));
        iter=(i-1)*2^(numberOfDim-2)+1:i*2^(numberOfDim-2);
        minPoint(iter, IDiff)=CMin(IDiff);
        subMin=in(in(:, IDiff)==CMin(IDiff), [1:IDiff-1 IDiff+1:end]);
        temp=getMaxDiffSub(subMin);
        minPoint(iter, [1:IDiff-1 IDiff+1:end])=temp;
        [CMin, CMax]=swap(CMin, CMax);%swap
    end
end
end
function [minPoint]=getMaxDiff(in)
[~, numberOfDim]=size(in);
[CMax, IMax] = max(in,[],  1);
[CMin,IMin] = min(in, [], 1);
if numberOfDim==1
    minPoint=CMin;
else
    mask=1:numberOfDim;
    minPoint=zeros(2^(numberOfDim), numberOfDim);
    [sortedX,sortingIndices] = sort(CMax-CMin,'descend');
    
    for i=1:2^(numberOfDim-1)
        IDiff=sortingIndices(ceil(i/2^(numberOfDim-2)));
        iter=(i-1)*2^(numberOfDim-2)+1:i*2^(numberOfDim-2);
        minPoint(iter, IDiff)=CMin(IDiff);
        subMin=in(in(:, IDiff)==CMin(IDiff), [1:IDiff-1 IDiff+1:end]);
        temp=getMaxDiffSub(subMin);
        minPoint(iter, [1:IDiff-1 IDiff+1:end])=temp;
        [CMin, CMax]=swap(CMin, CMax);%swap
    end
end
end
function [minPoint]=getMaxDiff3(in)
[~, numberOfDim]=size(in);
[CMax, IMax] = max(in,[],  1);
[CMin,IMin] = min(in, [], 1);
if numberOfDim==1
    minPoint=[CMin;CMax];
else
    mask=1:numberOfDim;
    minPoint=zeros(2^(numberOfDim), numberOfDim);
    [sortedX,sortingIndices] = sort(CMax-CMin,'descend');
    
    for i=1:2
        IDiff=sortingIndices(1);
        iter=(i-1)*2^(numberOfDim-1)+1:i*2^(numberOfDim-1);
        minPoint(iter, IDiff)=CMin(IDiff);
        subMin=in(:, [1:IDiff-1 IDiff+1:end]);
        temp=getMaxDiff3(subMin);
        minPoint(iter, [1:IDiff-1 IDiff+1:end])=temp;
        [CMin, CMax]=swap(CMin, CMax);%swap
    end
end
end
function [y,x]=swap(x,y)
end
% konstrukcja palety
% kazdy blok jest reprezentowany przez srednia kolorow wewnatrz niego
%   for i = 1 : block_num
%     block = cell2mat(blocks(i));
%     pal = [pal; mean(block)];
%   end