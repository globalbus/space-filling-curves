function mask=gaussMask(sigma, size)
x = linspace(1, size, size);
mask = exp(-x .^ 2 / (2 * sigma ^ 2));
mask = mask / sum (mask); % normalize
mask=mask(mask>2*eps);