function pal = equalSplit(~, num)
init=ceil(num^(1/3));
initialVector=floor(linspace(0,255,init));
comb=allcomb(initialVector, initialVector, initialVector);

if(num<init^3)
    half=floor(num/2);
    pal=comb([1:half end-half+1:end],:);
else
    pal=comb;
end
end