function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 23-Nov-2014 22:21:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;
set(handles.measureMenu, 'Value', 1);
measureMenu_Callback(handles.measureMenu, [], handles);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.mainWindow);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in openFileButton.
function openFileButton_Callback(hObject, eventdata, handles)
% hObject    handle to openFileButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName] = uigetfile({'*.tiff;*.jpg;*.jpeg;*.png;*.JPEG;*.JPG;*.bmp;*.BMP','All Image Files'},'Wybierz plik');
%if canceled
if FileName==0
    return;
end
%read image
I=imread(strcat(PathName, FileName));
%remove previous object
if(isfield(handles,'inputImage'))
    delete(handles.inputImage);
end
%create image object and associate with inputAxes
image(I, 'parent',handles.inputAxes, 'tag', 'inputImage');
%remove axis
set(handles.inputAxes, 'Visible','off');
%enable processing availablity
set(handles.processButton,'Enable','on');



% --- Executes on button press in processButton.
function processButton_Callback(hObject, eventdata, handles)
% hObject    handle to processButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%input image
I=get(get(handles.inputAxes, 'child'), 'CDATA');
%curve id
curve = get(handles.curveListbox,'Value');
%feedback factor
feedback = get(handles.feedbackSlider,'Value');
%weight factor
weight= get(handles.modSlider,'Value');
numberOfColors =str2double(get(handles.numberOfColorsMenu, 'String'));
numberOfColors=max(round(numberOfColors),0);
colorsMultiplier=get(handles.genAndEliminateMenu,'Value');
%colors from table

switch get(handles.measureMenu,'Value');
case 1
    colors=get(handles.colorTable, 'Data');
case 2
    colors=double(median_cutOrg(reshape(I, [], 3), numberOfColors*colorsMultiplier));
    if(colorsMultiplier>1)
        colors=double(eliminateNearestPointsInSet(colors, numberOfColors*(colorsMultiplier-1)));
    end
case 3
    colors=double(median_cutHull(reshape(I, [], 3), numberOfColors*colorsMultiplier));
    if(colorsMultiplier>1)
        colors=double(eliminateNearestPointsInSet(colors, numberOfColors*(colorsMultiplier-1)));
    end
case 4
    colors=double(median_cut(reshape(I, [], 3), numberOfColors*colorsMultiplier));
    if(colorsMultiplier>1)
        colors=double(eliminateNearestPointsInSet(colors, numberOfColors*(colorsMultiplier-1)));
    end
case 5
    colors=double(equalSplit([], numberOfColors*colorsMultiplier));
    if(colorsMultiplier>1)
        colors=double(eliminateNearestPointsInSet(colors, numberOfColors*(colorsMultiplier-1)));
    end
end

lutValues=str2num(get(handles.precisionPopup, 'String'));
lutPrecision=lutValues(get(handles.precisionPopup, 'Value'));


%run processing
[O, LUT]=coloring2(I,curve, feedback,weight,lutPrecision,colors);
addpath(fullfile('..'));
[mae, mse, peaktonoise, peakToNoiseBlur]=qualityComparator(I, O);
actualString =get(handles.peakToNoiseLabel, 'String');
actualString= regexprep(actualString, '\s*[\d|\d\.\d]+', '');
set(handles.peakToNoiseLabel, 'String', [actualString ' ', num2str(peaktonoise,'%2.1f')]);
actualString =get(handles.peakToNoiseBlur, 'String');
actualString= regexprep(actualString, '\s*[\d|\d\.\d]+', '');
set(handles.peakToNoiseBlur, 'String', [actualString ' ', num2str(peakToNoiseBlur,'%2.1f')]);

%remove old image
if(isfield(handles,'outputImage'))
    delete(handles.outputImage);
end
%create image object and associate with outputAxes
im=image(O, 'parent',handles.outputAxes, 'tag', 'outputImage');
%remove axis
set(handles.outputAxes, 'Visible','off');
%enable save button availablity
set(handles.saveButton,'Enable','on');
%save colortable
colorData.colors=colors;
colorData.LUT=LUT;
set(im,'UserData',colorData);




% --- Executes on selection change in curveListbox.
function curveListbox_Callback(hObject, eventdata, handles)
% hObject    handle to curveListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function curveListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to curveListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
data=[cellstr('od lewej do prawej'), cellstr('z góry na dół'), cellstr('od lewej do prawej z zawijaniem'), cellstr('z góry na dół z zawijaniem'), cellstr('na skos'), cellstr('Hilbert') cellstr('Peano') cellstr('Z-Order')];
set(hObject, 'String',data);
set(hObject, 'Value',length(data)-1);

% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName] = uiputfile({'*.jpg;*.jpeg;*.png;*.JPEG;*.JPG;*.bmp;*.BMP','All Image Files'},'Wybierz plik');
%if canceled
if FileName==0
    return;
end
I=get(get(handles.outputAxes, 'child'), 'CDATA');
colors=get(get(handles.outputAxes, 'child'), 'UserData');
colors=double(colors.colors)/255;
colors=transpose(colors);
%out=rgb2ind(I, colors);
%imwrite(out, colors,strcat(PathName, FileName));
imwrite(I,strcat(PathName, FileName));
% --- Executes on slider movement.
function feedbackSlider_Callback(hObject, eventdata, handles)
% hObject    handle to feedbackSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%additional information on text label
actualString =get(handles.feedbackLabel, 'String');
actualString= regexprep(actualString, '\s*[\d|\d\.\d]+', '');
set(handles.feedbackLabel, 'String', [actualString ' ', num2str(get(hObject,'Value'),'%2.0f')]);

% --- Executes during object creation, after setting all properties.
function feedbackSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to feedbackSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in colorsListbox.
function colorsListbox_Callback(hObject, eventdata, handles)
% hObject    handle to colorsListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%preselected color tables
rgbk=[255 0 0; 0 255 0;0 0 255;0 0 0];
cmyk=[255 255 0; 0 255 255;255 0 255;0 0 0];
bw=[255 255 255;0 0 0];
colors8=[255 255 0; 0 255 255;255 0 255;0 0 0;255 0 0; 0 255 0;0 0 255;255 255 255];
switch get(hObject, 'Value')
    case 1
        set(handles.colorTable, 'Data', rgbk);
    case 2
        set(handles.colorTable, 'Data', cmyk);
    case 3
        set(handles.colorTable, 'Data', bw);
    case 4
        set(handles.colorTable, 'Data', colors8);
end
% --- Executes during object creation, after setting all properties.
function colorsListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to colorsListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in colorTableAddButton.
function colorTableAddButton_Callback(hObject, eventdata, handles)
% hObject    handle to colorTableAddButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%add row at end of table
table=get(handles.colorTable, 'Data');
table=[table;[0,0,0]];
set(handles.colorTable, 'Data', table);


% --- Executes when selected cell(s) is changed in colorTable.
function colorTable_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to colorTable (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

%save data with selected row
set(hObject,'UserData',eventdata);

% --- Executes on button press in colorTableDelButton.
function colorTableDelButton_Callback(hObject, ~, handles)
% hObject    handle to colorTableDelButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%remove selected row
Index=get(handles.colorTable,'UserData');
[~,b]=size(Index);
if b==0
    return;
end
table=get(handles.colorTable, 'Data');
table(Index.Indices(:,1), :) = [];
set(handles.colorTable,'Data',table);


% --- Executes on slider movement.
function modSlider_Callback(hObject, eventdata, handles)
% hObject    handle to modSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%additional information on text label
actualString =get(handles.weightLabel, 'String');
actualString= regexprep(actualString, '\s*[\d|\d\.\d]+', '');
set(handles.weightLabel, 'String', [actualString, ' ', num2str(get(hObject,'Value'),'%2.1f')]);

% --- Executes during object creation, after setting all properties.
function modSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in measureMenu.
function measureMenu_Callback(hObject, eventdata, handles)
% hObject    handle to measureMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
state=get(hObject, 'Value');
switch state
case 1
    set(handles.text4, 'Visible', 'on');
    set(handles.colorsListbox, 'Visible', 'on');
    set(handles.colorTable, 'Visible', 'on');
    set(handles.colorTableAddButton, 'Visible', 'on');
    set(handles.colorTableDelButton, 'Visible', 'on');
    set(handles.numberOfColorsText, 'Visible', 'off');
    set(handles.numberOfColorsMenu, 'Visible', 'off');
    set(handles.genAndEliminateMenu, 'Visible', 'off');
    set(handles.genAndEliminateText, 'Visible', 'off');
otherwise
    set(handles.text4, 'Visible', 'off');
    set(handles.colorsListbox, 'Visible', 'off');
    set(handles.colorTable, 'Visible', 'off');
    set(handles.colorTableAddButton, 'Visible', 'off');
    set(handles.colorTableDelButton, 'Visible', 'off');
    set(handles.numberOfColorsText, 'Visible', 'on');
    set(handles.numberOfColorsMenu, 'Visible', 'on');
    set(handles.genAndEliminateMenu, 'Visible', 'on');
    set(handles.genAndEliminateText, 'Visible', 'on');
end

% --- Executes during object creation, after setting all properties.
function measureMenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to measureMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'Value',1.0);

% --- Executes when entered data in editable cell(s) in colorTable.
function colorTable_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to colorTable (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

%check input values, if not valid
if eventdata.NewData>255 || eventdata.NewData<0 || isnan(eventdata.NewData)
    %reverse to previous values
    table=get(hObject, 'Data');
    table(eventdata.Indices(1),eventdata.Indices(2)) = eventdata.PreviousData;
    set(hObject,'Data',table);
end



% --- Executes on selection change in precisionPopup.
function precisionPopup_Callback(hObject, eventdata, handles)
% hObject    handle to precisionPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns precisionPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from precisionPopup


% --- Executes during object creation, after setting all properties.
function precisionPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to precisionPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String',2.^(3:8));


% --- Executes on selection change in genAndEliminateMenu.
function genAndEliminateMenu_Callback(hObject, eventdata, handles)
% hObject    handle to genAndEliminateMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns genAndEliminateMenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from genAndEliminateMenu


% --- Executes during object creation, after setting all properties.
function genAndEliminateMenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to genAndEliminateMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String',1:3);



function numberOfColorsMenu_Callback(hObject, eventdata, handles)
% hObject    handle to numberOfColorsMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numberOfColorsMenu as text
%        str2double(get(hObject,'String')) returns contents of numberOfColorsMenu as a double
actualString =get(hObject, 'String');
actualString= regexprep(actualString, '[^\d]*', '');
set(hObject, 'String', actualString);

% --- Executes during object creation, after setting all properties.
function numberOfColorsMenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numberOfColorsMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', num2str(8));
