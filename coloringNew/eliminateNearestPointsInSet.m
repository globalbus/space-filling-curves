function pal = eliminateNearestPointsInSet(pal, n)
pal=double(pal);
% nor=zeros(length(pal), length(pal));
% for i=1:length(pal);
%     %get pixel color
%     %calculate distance from colors in array to pixel color
%     for j=i:length(pal);
%         nor(j, i)=norm(pal(j,:)-pal(i,:));
%     end
% end
nor=bsxfun(@plus,dot(pal',pal',1)',dot(pal',pal',1))-2*(pal*pal');
nor(eye(size(nor))~=0)=NaN;

for i=1:n
    %take color with minimum distance
    [C,I]=min(nor(:), [], 1);
    [X, Y]=ind2sub(size(nor), I);
    X=[X Y];
    [~,I]=min(sum(nor(X, :)')+sum(nor(:, X)));
    nor(X(I), :)=[];
    nor(:, X(I))=[];
    pal(X(I), :)=[];
end
pal=uint8(pal);
end