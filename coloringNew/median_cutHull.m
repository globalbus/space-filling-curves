% Kwantyzacja kolorów metoda median cut.
%
% nowa_paleta = median_cut(obraz, liczba_kolorow);
%

function pal = median_cutHull(in, num)
addpath(fullfile('..','compression'));
pal = zeros(num*2, 3);
block_num = 1;

% bloki przechowywane sa jako struktury
% pierwszy blok zawiera wszystkie kolory
blocks=cell(num, 1);
blocks(1)={in};
while block_num < num
    m_len = 0; % dlugosc najdluzszego boku
    m_idx = 0; % indeks koloru dla tego boku
    m_i = 0;   % indeks bloku z najdluzszym bokiem
    
    % szukamy najdluzszego boku wsrod wszystkch blokow
    for i = 1 : block_num
        cur = cell2mat(blocks(i));
        % dlugosci bokow dla trzech kanalow
        dif = max(cur) - min(cur);
        % dlugosc najdluzszego oraz indeks kanalu
        [m_len_, m_idx_] = max(dif);
        % zwyczajne poszukiwanie maksimum
        if m_len_ > m_len
            m_len = m_len_;
            m_i = i;
            m_idx = m_idx_;
        end
    end % for
    
    % wybrany blok
    max_block = cell2mat(blocks(m_i));
    
    % znajdujemy mediane
    max_block = sortrows(max_block, m_idx);
    med = floor((size(max_block, 1) + 1)/ 2);
    
    % dzielimy wybrany blok wg. mediany
    nblock1 = max_block(1 : med, :);
    nblock2 = max_block(med+1 : size(max_block, 1), :);
    if(isempty(nblock2))
        break;
    end
    % zamieniamy wybrany blok oraz dokladamy nowy
    blocks(m_i) = {nblock1};
    blocks(block_num+1)= {nblock2};
    block_num = block_num + 1;
end % while
% konstrukcja palety
% kazdy blok jest reprezentowany przez srednia kolorow wewnatrz niego
set=zeros(2,3);
for i = 1 : block_num
    block = cell2mat(blocks(i));
    %block=unique(block, 'rows');
    block=double(block);
    if(size(block,1)==1)
        pal(2*i-1:2*i, :)=[block;block];
        continue;
    end
    
    try
        conv=convhull(block);
        test=[block(conv(:,1), 1), block(conv(:,2),2), block(conv(:,3),3)];
        avg=sum(test)/length(test); %centre of convex hull
        D = bsxfun(@plus,dot(test',test',1)',dot(avg',avg',1))-2*(test*avg');
        [~, I]=max(D);
        set(1, :)=test(I, :);%furthest point from centre
        D = bsxfun(@plus,dot(test',test',1)',dot(set(1, :)',set(1, :)',1))-2*(test*set(1, :)');
        [~, I]=max(D);
        set(2, :)=test(I(1), :);%furthest point from first one - antipodal
    catch
        set=double(eliminateNearestPointsInSet(block, size(block,1)-2)); %if convex cannot be found
    end
    pal(2*i-1:2*i, :)=set;
end
pal=eliminateNearestPointsInSet(pal, length(pal)-num);
end