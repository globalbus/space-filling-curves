% Kwantyzacja kolorów metoda median cut.
%
% nowa_paleta = median_cut(obraz, liczba_kolorow);
%

function pal = median_cutOrg(in, num)

pal = zeros(num, 3);
block_num = 1;

% bloki przechowywane sa jako struktury
% pierwszy blok zawiera wszystkie kolory
blocks=cell(num, 1);
blocks(1)={in};
while block_num < num
    m_len = 0; % dlugosc najdluzszego boku
    m_idx = 0; % indeks koloru dla tego boku
    m_i = 0;   % indeks bloku z najdluzszym bokiem
    
    % szukamy najdluzszego boku wsrod wszystkch blokow
    for i = 1 : block_num
        cur = cell2mat(blocks(i));
        % dlugosci bokow dla trzech kanalow
        dif = max(cur) - min(cur);
        % dlugosc najdluzszego oraz indeks kanalu
        [m_len_, m_idx_] = max(dif);
        % zwyczajne poszukiwanie maksimum
        if m_len_ > m_len
            m_len = m_len_;
            m_i = i;
            m_idx = m_idx_;
        end
    end % for
    
    % wybrany blok
    max_block = cell2mat(blocks(m_i));
    
    % znajdujemy mediane
    max_block = sortrows(max_block, m_idx);
    med = floor((size(max_block, 1) + 1)/ 2);
    
    % dzielimy wybrany blok wg. mediany
    nblock1 = max_block(1 : med, :);
    nblock2 = max_block(med+1 : size(max_block, 1), :);
    
    % zamieniamy wybrany blok oraz dokladamy nowy
    blocks(m_i) = {nblock1};
    blocks(block_num+1)= {nblock2};
    block_num = block_num + 1;
end % while
% konstrukcja palety
% kazdy blok jest reprezentowany przez srednia kolorow wewnatrz niego
for i = 1 : block_num
    block = cell2mat(blocks(i));
    %lmin=min(block);
    %lmax=max(block);
    color=mean(block);
    %color(find(lmin==CMin))=CMin(lmin==CMin);
    %color(find(lmax==CMax))=CMax(lmax==CMax);
    pal(i, :) = color;
end
