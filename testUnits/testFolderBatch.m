function [resultsRice, resultsPng]=testFolderBatch(directory, outputDir)
listDir=dir(fullfile(directory,'*'));
listDir = listDir(cellfun(@(x) x==0,{listDir.isdir}));
filenames={listDir.name};
for i=1:length(filenames)
    filename=char(filenames(i));
    disp(['Processing ' filename ' ' num2str(i) ' of ' num2str(length(filenames)) ]);
    resultsRice(i)=testFileRice(fullfile(directory, filename), fullfile(outputDir, 'rice'), 1, 0);
end
for i=1:length(filenames)
    filename=char(filenames(i));
    disp(['Processing ' filename ' ' num2str(i) ' of ' num2str(length(filenames)) ]);
    resultsPng(i)=testFilePng(fullfile(directory, filename), fullfile(outputDir, 'png'));
end