function hFig=compareReadTimes(resultsRice, resultsPng)
riceTime=cell2mat({resultsRice(:).timeOfRead});
pngTime=cell2mat({resultsPng(:).timeOfRead});
hFig = figure();
packed=[riceTime;pngTime]';
bar(packed, 'group');
maxValue = ceil(max(reshape(packed,[],1))*10)/10;
axis([1-0.5 length(riceTime)+0.5 0 maxValue]);
set(gca,'XTick', 1:length(riceTime));
print(hFig, 'results.tiff');