function compareResults(orgDir,pngDir, outputDir)
listDir=dir(fullfile(orgDir,'*'));
listDir = listDir(cellfun(@(x) x==0,{listDir.isdir}));
listPngDir=dir(fullfile(pngDir,'*'));
listPngDir = listPngDir(cellfun(@(x) x==0,{listPngDir.isdir}));
listOutputDir=dir(fullfile(outputDir,'*'));
listOutputDir = listOutputDir(cellfun(@(x) x==0,{listOutputDir.isdir}));


orgSize=cell2mat({listDir.bytes});
pngSize=cell2mat({listPngDir.bytes});
newSize=cell2mat({listOutputDir.bytes});

compressionRatioRice=1-newSize./orgSize;
compressionRatioPng=1-pngSize./orgSize;
hFig = figure();
bar([compressionRatioRice; compressionRatioPng]', 'group');
print(hFig, 'results.tiff');