function hFig=compareRatio(resultsRice, resultsPng)
orgSize=cell2mat({resultsRice(:).orgSize});
newSize=cell2mat({resultsRice(:).newSize});
pngSize=cell2mat({resultsPng(:).newSize});
compressionRatioRice=1-newSize./orgSize;
compressionRatioPng=1-pngSize./orgSize;
hFig = figure();
packed=[compressionRatioRice; compressionRatioPng]';
bar(packed, 'group');
maxValue = ceil(max(reshape(packed,[],1))*10)/10;
axis([1-0.5 length(orgSize)+0.5 0 maxValue]);
set(gca,'XTick', 1:length(orgSize));
print(hFig, 'results.tiff');