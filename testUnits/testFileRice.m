function output=testFileRice(filename, outputDir, testIntegrity, testAll)

addpath(fullfile('..', 'compression'));
if(~exist(outputDir, 'dir'))
    mkdir(outputDir);
end
im=imread(filename);
tmpDir=dir(filename);
output.orgSize=tmpDir.bytes;
[~,name,~] = fileparts(filename);
size=8;
if(testAll==1)
    avg=zeros(1,size);
    timeOfWrite=zeros(1,size);
    newSizes=zeros(1,size);
    tocs=zeros(1,size);
    for i=1:size
        tic;
        filenameNew=fullfile(outputDir, [name '.' num2str(i) '.rice']);
        avg(i)=imwrite2(im, filenameNew, i);
        tocs(i)=toc;
        timeOfWrite(i)=toc;
        tmpDir=dir(filenameNew);
        newSizes(i)=tmpDir.bytes;
    end
    output.avg=avg;
    output.timeOfWrite=tocs;
    output.newSize=newSizes;
else
    filenameNew=fullfile(outputDir, [name '.rice']);
    tic;
    output.avg=imwrite2(im, filenameNew);
    output.timeOfWrite=toc;
    tmpDir=dir(filenameNew);
    output.newSize=tmpDir.bytes;
    output.V=calcV(im);
end
if(testIntegrity==1)
    tic;
    im2=imread2(filenameNew);
    output.timeOfRead=toc;
    if(~isequal(im, im2))
        disp('written image and read image was not the same! - Error on write/read functions');
    end
end

function v=calcV(im)
[~,v]=convhull(reshape(double(im), [], 3));