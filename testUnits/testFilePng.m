function output=testFilePng(filename, outputDir)
addpath(fullfile('..', 'compression'));
if(~exist(outputDir, 'dir'))
    mkdir(outputDir);
end
tmpDir=dir(filename);
output.orgSize=tmpDir.bytes;
im=imread(filename);
[~,name,~] = fileparts(filename);
filenameNew=fullfile(outputDir, [name '.png']);
tic;
imwrite(im, filenameNew);
output.timeOfWrite=toc;
tic;
tmpDir=dir(filenameNew);
output.newSize=tmpDir.bytes;
im2=imread(filenameNew);
output.timeOfRead=toc;
