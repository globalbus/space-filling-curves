function [mae, mse, peakToNoise, peakToNoiseBlur]=qualityComparator(im, im2)

diff=double(reshape(im, [], 1))-double(reshape(im2, [], 1));
mae=sum(abs(diff))/numel(im);
mse=sum(diff.^2)/numel(im);
d=double(intmax(class(im)));
peakToNoise=10*log10(d*d/mse);

%h =fspecial('gaussian',7,5);
h=fspecial('disk',3);
im2 = imfilter(im2, h);
im = imfilter(im, h);
diff=double(reshape(im, [], 1))-double(reshape(im2, [], 1));
mse2=sum(diff.^2)/numel(im);
peakToNoiseBlur=10*log10(d*d/mse2);