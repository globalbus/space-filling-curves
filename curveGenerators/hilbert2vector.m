function [x2,y2] = hilbert2vector(orgSize)
%smallest possible curve dimension
n1=ceil(log2(max(orgSize)));


[x,y]=hilbert_opt(n1);
%scaling to positive only values
n2=cast(bitsll(1,n1-1), class(x));
x=typecast((x+1)/2+n2, ['u' class(x)]);
y=typecast((y+1)/2+n2,['u' class(y)]);
%remove area outside the image
x2=x(x<=orgSize(1) & y<=orgSize(2));
y2=y(x<=orgSize(1) & y<=orgSize(2));