function [x2,y2] = zorder2vector(orgSize)
%smallest possible curve dimension
n1=ceil(log2(max(orgSize)));
[x,y]=zorder(n1);
%remove area outside the image
x2=x(x<=orgSize(1) & y<=orgSize(2));
y2=y(x<=orgSize(1) & y<=orgSize(2));