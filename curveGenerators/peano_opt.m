function [x,y] = peano_opt(n)
% based on hilbert_opt ideas, with different repetition rate 
% iterate version
if(n<4)
    type='int8';
elseif(n<10)
    type='int16';
%elseif(n<32) that size is totally useless and eats a GB of RAM.
%    type='int32';
else
    error('peano curve request is too large');
end
help=3^n;
x=zeros(help^2,1, type);
y=zeros(help^2,1, type);
xo=zeros((help/3)^2,1, type);
yo=zeros((help/3)^2,1, type);
offset=floor(help^2/2);
help=cast(help, type);
previous=1;
%[-1 0 1 1 0 -1 -1 0 1]
%[-1 -1 -1 0 0 0 1 1 1]

%[1 1 1 -1 -1 -1 1 1 1]
%[0 0 0 0 0 0 0 0 0]
%[0 0 0 0 0 0 0 0 0]
%[1 -1 1 1 -1 1 1 -1 1]

for i=1:n
  actual=ceil(3^i^2/2);
  x(offset-actual+2:offset+actual)=[-help+xo(1:previous);      +xo(1:previous);  help+xo(1:previous); help-xo(1:previous); -xo(1:previous); -help-xo(1:previous); -help+xo(1:previous); xo(1:previous); help+xo(1:previous)]/3;
  y(offset-actual+2:offset+actual)=[-help+yo(1:previous); -help-yo(1:previous); -help+yo(1:previous);      yo(1:previous); -yo(1:previous);       yo(1:previous);  help+yo(1:previous); help-yo(1:previous); help+yo(1:previous)]/3;
  previous=3^i^2;
  xo(1:previous)=x(offset-actual+2:offset+actual);
  yo(1:previous)=y(offset-actual+2:offset+actual);
end
