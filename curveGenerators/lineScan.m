function [x,y] = lineScan(orgSize)

x=repmat(uint32(1:orgSize(2)), 1, orgSize(1));
y=zeros(1, orgSize(1)*orgSize(2), 'uint32');
for i=1:orgSize(1)
    y(1+(i-1)*orgSize(2):i*orgSize(2))=i;
end