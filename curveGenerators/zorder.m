function [x,y] = zorder(n)
if(n<8)
    type='uint8';
elseif(n<16)
    type='uint16';
%elseif(n<32) that size is totally useless and eats a GB of RAM.
%    type='int32';
else
    error('hilbert curve request is too large');
end
help=bitsll(1,n);
x=zeros(help^2,1, type);
y=zeros(help^2,1, type);
x(1)=1;
y(1)=1;
previous=1;
for i=1:n
    helper=2^(i-1);
    helper2=2^i^2;
    x(1:helper2)=[x(1:previous);x(1:previous)+helper;x(1:previous);x(1:previous)+helper];
    y(1:helper2)=[y(1:previous);y(1:previous);y(1:previous)+helper;y(1:previous)+helper];
    previous=helper2;
end