function [curveVector] = getIndices(im, x, y, order)
orgSize=size(im);
ind=sub2ind(orgSize, uint32(x), uint32(y));
clear x;
clear y;
if(order<0)
    ind(ind)=1:length(ind);
end
if(length(orgSize)==3)
    curveVector=zeros(orgSize, class(im));
    curveVector=reshape(curveVector, [], 1);
    for i=1:orgSize(3)
        curveVector(1+(i-1)*length(ind):i*length(ind))=im(ind+(i-1)*length(ind));
    end
else
    curveVector=im(ind);
end
curveVector=reshape(curveVector, orgSize);
end

