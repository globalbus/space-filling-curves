function [x2,y2] = peano2vector(orgSize)
%smallest possible curve dimension
n=max(orgSize);
n1=ceil(log(n)/log(3));
[x,y]=peano_opt(n1);
%scaling to positive only values
n2=cast((3^n1-1)/2+1, class(x));
x=typecast(x+n2, ['u' class(x)]);
y=typecast(y+n2, ['u' class(y)]);
%remove area outside the image
x2=x(x<=orgSize(1) & y<=orgSize(2));
y2=y(x<=orgSize(1) & y<=orgSize(2));