function [x,y] = zShape(orgSize)

x=zeros(1, orgSize(1)*orgSize(2), 'uint32');
y=zeros(1, orgSize(1)*orgSize(2), 'uint32');
offset=1;
loops=orgSize(1)+orgSize(2)-1;
for i=1:loops
    length=min([min(orgSize(1:2)),i,loops-i+1]) ;
    xEnd=min([orgSize(1), i]);
    xStart=xEnd-length+1;
    yEnd=min([orgSize(2), i]);
    yStart=yEnd-length+1;
    if(mod(i,2)==1)
        x(offset:offset+length-1)=uint32(xStart:xEnd);
        y(offset:offset+length-1)=uint32(yEnd:-1:yStart);
    else
        x(offset:offset+length-1)=uint32(xEnd:-1:xStart);
        y(offset:offset+length-1)=uint32(yStart:yEnd);
    end
    offset=offset+length;
end