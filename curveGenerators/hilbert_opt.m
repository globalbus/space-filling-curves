function [x,y] = hilbert_opt(n)
%HILBERT Hilbert curve.
% modified version from laboratiories 
% iterate version
if(n<8)
    type='int8';
elseif(n<16)
    type='int16';
%elseif(n<32) that size is totally useless and eats a GB of RAM.
%    type='int32';
else
    error('hilbert curve request is too large');
end

help=bitsll(1,n);
x=zeros(help^2,1, type);
y=zeros(help^2,1, type);
xo=zeros((help/2)^2,1, type);
yo=zeros((help/2)^2,1, type);
offset=help^2/2;
help=cast(help, type);
% prod1=cast([0 1 1 0], type);
% prod2=cast([1 0 0 -1], type);
% prod3=prod2;
% prod4=prod1;
% helpVect1=cast([-1 -1 1 1], type);
% helpVect2=cast([-1 1 1 -1], type);
% cubeDim=4;
previous=1;
for i=1:n-1
    actual=2^i^2/2;
    %x(offset-actual+1:offset+actual)=(reshape(repmat(prod1,previous,1),[], 1).*repmat(xo(1:previous),cubeDim,1)+reshape(repmat(prod2,previous,1), [], 1).*repmat(yo(1:previous),cubeDim,1)+reshape(repmat(helpVect1,previous,1), [], 1)*help)/2;
    %y(offset-actual+1:offset+actual)=(reshape(repmat(prod3,previous,1),[], 1).*repmat(xo(1:previous),cubeDim,1)+reshape(repmat(prod1,previous,1), [], 1).*repmat(yo(1:previous),cubeDim,1)+reshape(repmat(helpVect2,previous,1), [], 1)*help)/2;
    x(offset-actual+1:offset+actual)=[-help+yo(1:previous); -help+xo(1:previous); help+xo(1:previous);  help-yo(1:previous)]/2;
    y(offset-actual+1:offset+actual)=[-help+xo(1:previous);  help+yo(1:previous); help+yo(1:previous); -help-xo(1:previous)]/2;
    previous=actual*2;
    xo(1:previous)=x(offset-actual+1:offset+actual);
    yo(1:previous)=y(offset-actual+1:offset+actual);
end
x=[-help+yo; -help+xo; help+xo;  help-yo]/2;
y=[-help+xo;  help+yo; help+yo; -help-xo]/2;