function [x,y] = selectCurve(orgSize, id)
switch id
    case 1
        [x,y]=lineScan(orgSize([2 1 3]));
    case 2
        [y,x]=lineScan(orgSize);
    case 3
        [x,y]=sShape(orgSize([2 1 3]));
    case 4
        [y,x]=sShape(orgSize);    
    case 5
        [y,x]=zShape(orgSize([2 1 3])); 
    case 6
        [x,y]=hilbert2vector(orgSize);
    case 7
        [x,y]=peano2vector(orgSize);
    case 8
        [x,y]=zorder2vector(orgSize);
    otherwise
        error(['curve id ',num2str(curve), ' does not supported']);
end