function [x,y] = sShape(orgSize)

x=zeros(1, orgSize(1)*orgSize(2), 'uint32');
y=zeros(1, orgSize(1)*orgSize(2), 'uint32');
for i=1:orgSize(1)
    if(mod(i,2)==1)
        x(1+(i-1)*orgSize(2):i*orgSize(2))=uint32(1:orgSize(2));
    else
        x(1+(i-1)*orgSize(2):i*orgSize(2))=uint32(orgSize(2):-1:1);
    end
    y(1+(i-1)*orgSize(2):i*orgSize(2))=i;
end