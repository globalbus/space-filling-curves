function output = coloring(im, curve, feedback, weight, func, colors)
% output     output image
% im         input image
% curve      curve id (allowed 1 and 2)
% feedback   feedback factor (from 0 to 1)
% mod        weight factor
% func       dimension function
% colors     allowed colors array
addpath(fullfile('..', 'curveGenerators'));
tic
%take size
orgSize=size(im);
%curve type selection
[x, y]=selectCurve(orgSize, curve);
toc
tic
%feedback vector
E=zeros(1,3);
[~,s]=size(colors);
curveVector=getIndices(im, x, y, 1); %vectorize by curve
curveVector=reshape(curveVector, [], orgSize(3)); %vector of pixels
colors=colors';
%helper for colors distance
nor=zeros(1,s);
for i=1:(length(curveVector));
    %get pixel color
    temp=double(curveVector(i, :));
    %add feedback
    e=temp+E;
    %calculate distance from colors in array to pixel color 
    for j=1:s
        nor(j)=func(e-colors(j,:));
    end
%     h1=repmat(e,s,1)-colors;
%     nor=sqrt(sum(h1.^2,2));
    %take color with minimum distance
    [~,I]=min(nor);
    A=colors(I, :);
    %save value in image
    curveVector(i, :)=A;
    %prepare feedback value
    E=temp/weight-A+E*feedback;
end
toc
output=getIndices(reshape(curveVector, orgSize), x, y, -1); %vectorize by curve
